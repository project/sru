<?php

/**
 * @file
 * @brief
 * Implements SRU Server in a Drupal environment.
 *
 * Class for generating the 'explain' response for an SRU request
 *
 * @package 	sru
 * @subpackage	
 * @author		
 */

_sru_load_response_file();

class sru_explain extends sru_response {

    public function __construct($sru) {
        parent::__construct($sru);
    }

    //---------------------------------------------

    /**
     * Collect the data for the Explain Response
     * and then convert to XML for output
     */

    protected function response() {

        $data = array(
            '#xmlns:srw' => 'http://www.loc.gov/zing/srw/',
            'srw:version' => array('@content' => $this->sru->version),
            'srw:record' => $this->response_record()
            );
        return theme('xml_sru', 'srw:explainResponse', $data); // FIXME: dependency to ns module
    }

    //---------------------------------------------

    /**
     * Collect the data for the Explain Response
     * and then convert to XML for output
     */

    protected function response_record() {

        $data = array(
            'srw:recordSchema' => array('@content' => 'http://explain.z3950.org/dtd/2.1/'),
            'srw:recordPacking' => array('@content' => $this->sru->recordPacking),
            'srw:recordData' => $this->response_record_data()
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Collect the data for the Explain Response
     * and then convert to XML for output
     */

    protected function response_record_data() {

        $data = array(
            'explain' => $this->response_explain_data()
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the content data for the response record
     */

    private function response_explain_data() {
        $data = array(
            '#xmlns' => 'http://explain.z3950.org/dtd/2.0/',
            'serverInfo' => $this->explain_server_info(),
            'databaseInfo' => $this->explain_database_info(),
//			'metaInfo' => $this->explain_meta_info(),
            'indexInfo' => $this->explain_index_info(),
//			'recordInfo' => $this->explain_record_info(),
            'schemaInfo' => $this->explain_schema_info(),
//			'configInfo' => $this->explain_config_info(),
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the serverInfo
     */

    private function explain_server_info() {
        $data = array(
            '#protocol' => 'SRW/U',
            '#transport' => 'http',
            '#version' => '0.1',
            'host' => array('@content' => $this->host),
            'port' => array('@content' => $this->port),
            'database' => array('@content' => 'sru'),
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the databaseInfo
     */

    private function explain_database_info() {
        $data = array(
            'title' => array(
                '#lang' => 'en',
                '#primary' => 'true',
                '@content' => variable_get('sru_depository_title', ''),
                ),
            'description' => array(
                '#lang' => 'en',
                '#primary' => 'true',
                '@content' => 'SRW/SRU/Z39.50 Gateway to ' . variable_get('site_name', 'Drupal') . ' Z39.50 Server',
                ),
//			'author' => array('@content' => 'author'),
//			'extent' => array('@content' => 'some description'),
//			'langUsage' => array('@content' => 'en cy')
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the metaInfo
     */

    private function explain_meta_info() {
        $data = array(
            'dateModified' => array('@content' => date('Y-M-d h:m:s', time())),
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the databaseInfo
     */

    private function explain_index_info() {
        $data = array(
            '+set' => $this->explain_index_info_sets(),
            '+index' => $this->explain_index_info_indexes(),
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the index sets
     */

    private function explain_index_info_sets() {

        $data = array();
        contextset_build();
        foreach (contextSet::all_set_info() as $name => $ident) {
            $data[] = array(
                '#identifier' => $ident,
                '#name' => $name
                );
        }
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the index indexes
     */

    private function explain_index_info_indexes() {
        static $x = 1;

        $data = array();
        contextset_build();
        foreach (contextSet::all_index_info() as $stuff) {
            $data[] = array(
                '#id' => 'id'.$x++,
                'title' => array(
                    '@content' => $stuff['title'],
                    '#lang' => 'en'
                    ),
                'map' => array(
                    'name' => array(
                        '#set' => $stuff['set'],
                        '@content' => $stuff['name']
                        )
                    )
                );
        }
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the record Info
     */

    private function explain_record_info() {
        $data = array(
            'recordSyntax' => array(
                '#identifier' => '1.2.840.10003.5.109.10',
                '+elementSet' => $this->explain_record_info_elements()
                )
            );
        return $data;
    }

    private function explain_record_info_elements() {
        $data[] = array(
            '#name' => 'F',
            'title' => array(
                '#lang' => 'en',
                '#primary' => 'true',
                '@content' => 'Full XML Record'
                ),
            );
        $data[] = array(
            '#name' => 'S',
            'title' => array(
                '#lang' => 'en',
                '#primary' => 'true',
                '@content' => 'Abbreviated XML Record'
                ),
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the schema Info
     */

    private function explain_schema_info() {
        $data = array(
            '+schema' => $this->explain_schema_info_schemas()
            );
        return $data;
    }

    private function explain_schema_info_schemas() {
    $filename = variable_get('sru_schema_file', key(sru_get_file_schemas()));
        $data[] = array(
            '#identifier' => variable_get('sru_schema_location', "http://{$_SERVER['HTTP_HOST']}") . base_path() . 'sru/schemas/' . $filename . '/0.0',
            '#location' => variable_get('sru_schema_location', "http://{$_SERVER['HTTP_HOST']}") . base_path() . 'sru/schemas/' . $filename . '.xsd',
            '#name' => $filename,
            '#sort' => 'false',
            '#retrieve' => 'true',
            'title' => array(
                '#lang' => 'en',
                '@content' => variable_get('site_name', 'Drupal'),
                )
            );
        return $data;
    }

    //---------------------------------------------

    /**
     * Generate the data for the config Info
     */

    private function explain_config_info() {
        $data = array(
            '+default' => $this->explain_config_info_defaults(),
            '+setting' => $this->explain_config_info_settings(),
            '+supports' => $this->explain_config_info_supports()
            );
        return $data;
    }

    private function explain_config_info_defaults() {
        $data = array();
        $data[] = array('#type' => 'numberOfRecords', '@content' => sru_defaults::numberOfRecords);
//		$data[] = array('#type' => 'sortSchema', '@content' => sru_defaults::sortSchema);
        $data[] = array('#type' => 'retrieveSchema', '@content' => sru_defaults::retrieveSchema);
        $data[] = array('#type' => 'resultSetTTL', '@content' => sru_defaults::resultSetTTL);
//		$data[] = array('#type' => 'stylesheet', '@content' => sru_defaults::stylesheet);
        $data[] = array('#type' => 'recordPacking', '@content' => sru_defaults::recordPacking);
        $data[] = array('#type' => 'numberOfTerms', '@content' => sru_defaults::numberOfTerms);
        return $data;
    }

    private function explain_config_info_settings() {
        $data = array();
        $data[] = array('#type' => 'contextSet', '@content' => contextSet::get_cs());
        $data[] = array('#type' => 'index', '@content' => contextSet::get_csi());
        $data[] = array('#type' => 'relation', '@content' => contextSet::get_csr());
        $data[] = array('#type' => 'maximumRecords', '@content' => sru_defaults::maximumRecords);
        return $data;
    }

    private function explain_config_info_supports() {
        $data = array();
        $supported = explode(' ', sru_defaults::supported());
        foreach ($supported as $item) {
            list($name, $mod) = explode(':', $item);
            $data[] = array('#type' => $name, '@content' => $mod);
        }
        return $data;
    }
}


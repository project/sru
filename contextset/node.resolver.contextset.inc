<?php

/**
 * @file
 * @brief
 * The set of CQL IRT resolvers.
 *
 * Defines classes that extend irtResolver.
 *
 * @package 	CQL
 * @subpackage	resolvers
 * @author
 */

_cql_load_resolver_file();

class nodeIrtResolverMySQL extends irtResolver {

  /**
   * Description needed.
   */
  public function resolveIndex(&$irtPack, $idx, $i, &$r, $t, &$joins) {
    $cs = contextSet::get_context_set('node');
    switch ($idx) {
      case 'recordtag':
        $r->set_prefix('node');
        $this->relType = 'node.tag' ;
        break;
      case 'nodetype':
        $r->set_prefix('node');
        $this->relType = 'node.nodetype' ;
        break;
      case 'body':
      case 'title':
      case 'nid':
        $irtPack->add_index($cs->abbv . '.' . $idx);
        $joins[$cs->dbjoin] = NULL;
        $this->relType = 'integer' ;
        break;
      default:
        throw new Exception(t('The node index "!idx" is not implemented', array('!idx' => $idx)));
        break;
    }
  }

  /**
   * Create the MySQL code given the term and operation
   * The syntactical check to ensure the term is a single or multiple
   * has already been done when the CQL was parsed
   */
  public function resolveRelation(&$irtPack, $rel, $i, $r, $t) {
    switch ($this->relType) {
      case 'node.tag':
        $this->resolveRelationTag($irtPack, $rel, $i, $r, $t);
        break;
      case 'node.nodetype':
        $this->resolveRelationNodeType($irtPack, $rel, $i, $r, $t);
        break;
    }
  }

  /**
   * Description needed.
   */
  private function resolveRelationTag(&$irtPack, $rel, $i, $r, $t) {
    $op = '' ;
    $cs = contextSet::get_context_set('node');
    $terms = $cs->irtTerm_Split((string)$t);
    $mods = $r->get_modifiers();
    switch ($rel) {
      case '=':
      case 'is':
        $op = $this->select($terms[0], $mods);
        break;
      case 'isnt':
        $op = $this->select($terms[0], $mods, TRUE);
        break;
      case 'isanyof':
        $op = $this->select_all($terms, $mods, 'OR');
        break;
      case 'isallof':
        $op = $this->select_all($terms, $mods, 'AND');
        break;
      case 'isnoneof':
        $op = $this->select_all($terms, $mods, 'AND', TRUE);
        break;
      default:
        throw new Exception(t('The relation "!rel" is not implemented for index "!idx"', array('!rel' => $rel, '!idx' => $i)));
        break;
    }
    $irtPack->add_output($op);
    $irtPack->set_done();
  }

  /**
   * Description needed.
   */
  private function resolveRelationNodeType(&$irtPack, $rel, $i, $r, $t) {
    $op = '' ;
    $cs = contextSet::get_context_set('node');
    $terms = $cs->irtTerm_SplitNodeTypes((string)$t);
    $mods = $r->get_modifiers();
    switch ($rel) {
      case '=':
      case '==':
        $op = $this->selectNodeTypes($terms, $mods);
        break;
      case '<>':
        $op = $this->selectNodeTypes($terms, $mods, TRUE);
        break;
      default:
        throw new Exception(t('The relation "!rel" is not implemented for index "!idx"', array('!rel' => $rel, '!idx' => $i)));
        break;
    }
    $irtPack->add_output($op);
    $irtPack->set_done();
  }

  /**
   * Build a combination of WHERE expressions for the supplied taxonomy terms
   * linked by the supplied operator
   */
  private function select_all($terms, &$mods, $func, $not = FALSE) {
    $first = TRUE;
    foreach ($terms as $term) {
      if (!$first) {
        $op .= ' ' . $func . ' ';
      }
      else {
        $first = FALSE;
      }
      $op .= $this->select($term, $mods, $not);
    }
    return $op;
  }

  /**
   * Build a WHERE expression for the supplied taxonomy term
   */
  private function select($term, &$mods, $not = FALSE) {
    static $x = 0;

    $tids = $this->global_term($term, $mods);
    if (count($tids)) {
      $not = $not ? 'NOT ' : '';
      $op = "
      (
        n.nid $not IN (
          SELECT DISTINCT(tn$x.nid)
          FROM {term_data} td$x
            INNER JOIN {term_node} tn$x ON (td$x.tid=tn$x.tid)
            WHERE (tn$x.tid IN (" . implode(',', $tids) . "))
        )
      )
      ";
      $x++;
      return $op;
    }

    throw new Exception(t('Unable to continue because term "!term", or vocabulary "!voc", could not be found', array('!term' => $term[1], '!voc' => $term[0])));
  }

}

class nodeIrtResolverLucene extends irtResolver {

  /**
   * Description needed.
   */
  public function resolveIndex(&$irtPack, $idx, $i, &$r, $t, &$joins) {
    $cs = contextSet::get_context_set('node');
    switch ($idx) {
      case 'recordtag':
        $r->set_prefix('node');
        $this->relType = 'node.tag' ;
        break;
      case 'nodetype':
        $r->set_prefix('node');
        $this->relType = 'node.nodetype' ;
        break;
      case 'body':
      case 'title':
        $irtPack->add_index('text');
        break;
      default:
        throw new Exception(t('The node index "!idx" is not implemented', array('!idx' => $idx)));
        break;
    }
  }

  /**
   * Create the MySQL code given the term and operation
   * The syntactical check to ensure the term is a single or multiple
   * has already been done when the CQL was parsed
   */
  public function resolveRelation(&$irtPack, $rel, $i, $r, $t) {
    $op = '' ;
    $cs = contextSet::get_context_set('node');
    $terms = $cs->irtTerm_Split_Lucene((string)$t);
    $mods = $r->get_modifiers();

    switch ($rel) {
      case '=':
      case 'is':
        $op = $this->select($terms[0], '', $mods);
        break;
      case 'isnt':
        $op = $this->select($terms[0], '-', $mods);
        break;
      case 'isoneof':
      case 'isanyof':
        $op = $this->select_all($terms, '', ' OR ', $mods);
        break;
      case 'isallof':
        $op = $this->select_all($terms, '', ' AND ', $mods);
        break;
      case 'isnoneof':
        $op = $this->select_all($terms, '-', ' AND ', $mods);
        break;
      default:
        throw new Exception(t('The node relation "!rel" is not implemented in Lucene', array('!rel' => $rel)));
        break;
    }
    $irtPack->add_output($op);
    $irtPack->set_done();
  }

  /**
   * Convert an array of terms into the equivalent Lucene
   */
  private function select_all($terms, $op, $rel, $mods = NULL, $index = 'taxonomy_guid') {

    $output = array();
    $join = ($op == '-') ? ' AND ' : ' OR ' ;

    foreach ($terms as $term) {
      $group = FALSE;


      if ($index) {
        if (drupal_substr($index, -1) != ':') {
          $index .= ':';
        }
      }
      $combine = $op . $index . implode($join . $op . $index, $tids);
      $output[] = $group ? '(' . $combine . ')' : $combine;
    }

    return implode($rel, $output);
  }

 
  /**
   * Build an expression for the supplied taxonomy term
   * Does not currently take any notice of descendants/antecedants
   */
  private function select($term, $op, $mods = NULL, &$index = 'taxonomy_guid') {
    return $this->select_all(array($term), $op, ' AND ', $mods, $index);
  }

  /**
   * Build a WHERE expression for the supplied nodetypes
   */
  private function selectNodeTypes($types) {
    if (count($types)==1) {
      $typelist = array_shift($types);
    }
    else {
      $typelist = '(' . implode(',', $types) . ')' ;
    }
    return 'type:' . $typelist;
  }
}

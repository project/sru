<?php

/**
 *
 * @file
 * @brief Defines generic functionality that currently doesn't warrant its own module.
 * @package contextset
 * @subpackage
 * @author
 */

class nodeContextSet extends contextSet {
    /**
     * nodeContextSet::__construct()
     *
     * @param mixed $data
     */
    protected function __construct($data) {
        parent::__construct('node', 'info:srw/cql-context-set/1/cql-v1.2', $data);
    }

    /**
     * All the data initialisation routines
     */

    /**
     * nodeContextSet::indexes()
     *
     * @return
     */
    protected function indexes() {
        return array(
            'recordtag' => NULL,
            'nodetype' => NULL,
            'title' => NULL,
            'body' => NULL,
            'nid' => NULL,
            );
    }

    /**
     * nodeContextSet::rels()
     *
     * @return
     */
    protected function rels() {
        return array(
            'is' => NULL,
            'isnt' => 'is',
            'isanyof' => NULL,
            'isallof' => 'isanyof',
            'isnoneof' => 'isanyof',
        );
    }

    /**
     * nodeContextSet::rmods()
     *
     * @return
     */
    protected function rmods() {
        return array('height', 'depth');
    }

    /**
     * nodeContextSet::bools()
     *
     * @return
     */
    protected function bools() {
        return array('xor');
    }

    /**
     * nodeContextSet::bmods()
     *
     * @return
     */
    protected function bmods() {
        return array();
    }

    /**
     * Syntax checking routines
     */

    /**
     * nodeContextSet::irtCheck_term()
     *
     * @param mixed $i
     * @param mixed $r
     * @param mixed $t
     * @return
     */
    protected function irtCheck_term($i, $r, $t) {
        list($rcs, $rrel) = explode('.', $r);
        if ($rcs != $this->name) {
            throw new Exception(t('Cannot use operation "!op" with index "!term"', array('!op' => $r, '!term' => $i)));
        }
    }

    /**
     * nodeContextSet::irtCheck_is()
     *
     * @param mixed $i
     * @param mixed $r
     * @param mixed $t
     * @return
     */
    protected function irtCheck_is($i, $r, $t) {
        $term = (string) $t;
        # Field Allow quoted strings
        # if (($term[0]=='"') && ($term[strlen($term)-1]=='"')) {
        # throw new Exception(t('Cannot use a quoted string as a term identifier with "!op"', array('!op' => $r)));
        # }
        $term = trim(trim(trim($term), '"'));
        $this->parseTag($term);
    }

    /**
     * nodeContextSet::parseTag()
     *
     * @param mixed $term
     * @return
     */
    private function parseTag($term) {
        /**
         * Peter Field changed the separator for the term explosion from : to ^
         */
        $bits = explode('^', $term);
        switch (count($bits)) {
            case 0:
            case 1:
                throw new Exception(t('A record tag must be composed of a "vocabulary^term", (!term supplied)', array('!term' => $term)));
                break;
            case 2:
                $a = $bits[0];
                $b = $bits[1];
                if (($b == '') || ($a == '')) {
                    throw new Exception(t('A record tag must be composed of a "vocabulary^term", (!term supplied)', array('!term' => $term)));
                }
                break;
            default:
                throw new Exception(t('Too many ":" in record tag, (!term supplied)', array('!term' => $term)));
                break;
        }
    }

    /**
     * nodeContextSet::irtCheck_isanyof()
     *
     * @param mixed $i
     * @param mixed $r
     * @param mixed $t
     * @return
     */
    protected function irtCheck_isanyof($i, $r, $t) {
        $term = (string) $t;
        if (!(($term[0] == '"') && ($term[strlen($term) - 1] == '"'))) {
            throw new Exception(t('Must have a quoted string as a term identifier with "!op"', array('!op' => $r)));
        }

        $terms = explode(',', substr($term, 1, strlen($term) - 2));
        foreach ($terms as $term) {
            $this->parseTag($term);
        }
    }

    /**
     * Build an array of vocab/term arrays from the supplied CQL term
     */

    public function irtTerm_split($term) {
        $bits = array();

        $term = trim(trim(trim($term), '"'));
        $terms = explode(',', $term);
        foreach ($terms as $term) {
            $bits[] = explode('^', trim($term));
        }
        return $bits;
    }

    /**
     * Build an array of vocab/term arrays from the supplied CQL term
     */

    public function irtTerm_split_Lucene($term) {
        $bits = array();

        $term = trim(trim(trim($term), '"'));
        $terms = explode(',', $term);
        foreach ($terms as $term) {
            $bits[] = '"' . trim($term) . '"';
        }
        return $bits;
    }
}


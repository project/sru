<?php

/**
 * @file
 * @brief
 * Implement Common Query Language in a Drupal environment.
 *
 * Defines the sru_searchretrieve response class that extends sru_response.
 * The parser is held in a separate file and is only loaded if needed.
 *
 * @package 	CQL
 * @subpackage
 * @author
 */

_sru_load_response_file();

class sru_searchretrieve extends sru_response {
  const MAXRECS = 5;
  const STARTREC = 1;
  const IDLETIME = 300;

  private $query = NULL;
  private $query2 = NULL;
  private $where = NULL;
  private $nids = array();
  private $exec = NULL;

  /**
   * Constructor parses the query and stores it
   */
  public function __construct($sru) {
    parent::__construct($sru);

    $this->query = $sru->query;
    if ($this->query) {
      $this->parsequery();
    }
    else {
      $this->add_error(sru_error::MANDATORY_PARAMETER_NOT_SUPPLIED, 'Missing or empty mandatory parameter', 'query');
    }
  }

  /**
   * Process the supplied query
   *
   * Parse the file into the internal object structure and then send
   * a visitor to go to every node and generate the MySQL output.
   *
   * Both CQLParser and the Executor object cache results
   *
   */
  public function parsequery() {
    _cql_load_parser_file();
    _cql_load_mysqlvisitor_file();
    _cql_load_executor_file();

    try {

      $this->exec = cqlExecutor::factory($this->query, array('nid'), cqlExecutor::MYSQL);
      /*
       $parser = new CQLParser($this->query);
       $parsed = $parser->parsequery();

       $visitor = new cqlVisitorGenMySQL();
       $parsed->accept($visitor);
       $this->query2 = (string) $visitor;
       $this->exec = new cqlExecutorMySQL($parser->cid(), (string) $visitor, TRUE);
       #*/
      $this->nids = $this->exec->fetch($this->sru->startrecord, $this->sru->maximumrecords);

    } catch (Exception $e) {
      drupal_set_message($e->getTraceAsString(), 'error');
      watchdog('SRU trace', '%message', array('%message' => $e->getMessage()));
      $this->add_error(sru_error::ERR_CQL_VOCABULARY, $e->getMessage(), 'cql');
    }
  }

  /**
   * Perform the search and issue the response
   *
   * @todo cache queries and results based on resultset ID
   */
  protected function response() {
    $data = array(
            '#xmlns' => variable_get('sru_schema_location', "http://{$_SERVER['HTTP_HOST']}"),
            '#xmlns:zs' => 'http://www.loc.gov/zing/srw/',
            '#xmlns:diag' => 'http://www.loc.gov/zing/srw/diagnostic/',
            '#xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            '#xsi:schemaLocation' => variable_get('sru_schema_location', "http://{$_SERVER['HTTP_HOST']}") . ' schema/content_node.xsd',
            '#xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
            '#xmlns:dcterms' => 'http://purl.org/dc/terms/',
            '#xmlns:lom' => 'http://ltsc.ieee.org/xsd/LOM',
            'zs:version' => array('@content' => $this->sru->version),
            'zs:numberOfRecords' => array('@content' => $this->exec->total()),
            'zs:records' => $this->fetchAllRecords(),
    );
    $xml = theme_xml_sru('zs:searchRetrieveResponse', $data);
    return $xml;
  }

  /**
   * Do the record search and return a block of records
   */
  private function fetchAllRecords() {
    $data = array();
    $maxrecs = ($this->sru->maxrecords) ? $this->sru->maxrecords : sru_searchretrieve::MAXRECS;
    $startrec = ($this->sru->startrecord) ? $this->sru->startrecord : sru_searchretrieve::STARTREC;

    $nids = $this->nids; // array_slice($this->nids, $startrec, $maxrecs);
    if (count($nids)) {
      $data['+zs:record'] = $this->fetchRecords($nids, $startrec);
    }

    return $data;
  }

  /**
   * Description needed.
   */
  private function fetchRecords($nids, $startrec) {
    $data = array();
    foreach ($nids as $nid) {
      $data[] = $this->fetchRecord($nid, $startrec++);
    }
    return $data;
  }

  /**
   * Description needed.
   */
  private function fetchRecord($nid, $posn) {
    $data = array(
            'zs:recordSchema' => array('@content' => 'info:srw/schema/1/dc-v1.1'),
            'zs:recordPacking' => array('@content' => $this->sru->recordpacking),
            'zs:recordData' => array('srw_dc:dc' => $this->fetchRecordData($nid)),
            'zs:recordIdentifier' => array('@content' => $nid),
            'zs:recordPosition' => array('@content' => $posn)
    );
    return $data;
  }

  /**
   * Get the data for each individual record
   *
   * Rally:US368:TA249
   * Modified to use module_invoke to fetch node-related 'recordData'
   * which allows it to be more easily extensible with much less
   * hard-wiring (which is bad)
   */
  private function fetchRecordData($nid) {
    $node = node_load($nid);
    // Trac #1178.  Encoding ampersands in the tagging tool notes field to prevent invalid xml
    $node->tagfields['notes'] = htmlspecialchars($node->tagfields['notes']);
    $data = array('#xmlns:srw_dc' => 'info:srw/schema/1/dc-schema');

    $results = module_invoke_all('sru', 'recorddata', $node);

    if (!empty($results)) {
      foreach ($results as $module => $stuff) {
        $data = array_merge($data, $stuff);
      }
    }
    return $data;
  }

  /**
   * Description needed.
   */
  static function testSearchRetrieve($query = 'ns.title = x') {
    _sru_load_request_file();
    $sru = new sru_request(array('query' => $query));
    $xr = new sru_searchretrieve($sru);
    return $xr->response();
  }

  /**
   * Description needed.
   */
  static function testRecordData(&$xr, $nid) {
    return $xr->fetchAllRecords($nid);
  }
}


<?php

/**
 * @file
 * @brief
 * Implements SRU Server in a Drupal environment.
 *
 * Error response class for SRU requests.
 * Handles multiple errors correctly.
 *
 * @package 	sru
 * @subpackage	
 * @author		
 */

_sru_load_response_file();

class sru_error extends sru_response {

    const SRU_ERROR_CODE = 0;
    const SRU_ERROR_MESG = 1;
    const SRU_ERROR_DETL = 2;

    const DIAGNOSTIC_URI = 'info:srw/diagnostic/1/' ;
    const DIAGNOSTIC_NS = 'http://www.loc.gov/zing/srw/diagnostic/' ;

    protected $errors;

    public function __construct($sru, $error = FALSE, $msg = FALSE, $detail = FALSE) {
        parent::__construct($sru);

        if ($error !== FALSE) {
            $this->add($error, $msg, $detail);
        }
    }

    //---------------------------------------------

    public function add($error, $msg = FALSE, $detail = FALSE) {
        $this->errors[] = array(
            self::SRU_ERROR_CODE => $error,
            self::SRU_ERROR_MESG => $msg,
            self::SRU_ERROR_DETL => $detail,
        );
    }

//-----------------------------------------------------------------------------

    protected function response() {
        $data = array(
            '#xmlns' => variable_get('sru_schema_location', "http://{$_SERVER['HTTP_HOST']}"),
            '#xmlns:zs' => 'http://www.loc.gov/zing/srw/',
            '#xmlns:diag' => 'http://www.loc.gov/zing/srw/diagnostic/',
            '#xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            '#xsi:schemaLocation' => variable_get('sru_schema_location', "http://{$_SERVER['HTTP_HOST']}") . ' schema/content_node.xsd',
            '#xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
            '#xmlns:dcterms' => 'http://purl.org/dc/terms/',
            '#xmlns:lom' => 'http://ltsc.ieee.org/xsd/LOM',
            '+diagnostics' => $this->diagnostics()
        );
        $xml = theme('xml_sru', 'zs:searchRetrieveResponse', $data);
        return $xml;
    }

    /**
     * Generate disagnostic for non-fatal error
     */

    protected function diagnostics() {
        $data = array();
        foreach ($this->errors as $e) {
            $data[] = $this->diagnostic($e);
        }
        return $data;
    }

    /**
     * Generate disagnostic for non-fatal error
     */

    protected function diagnostic($e) {
        $data = array(
            'diagnostic' => array(
                '#xmlns' => self::DIAGNOSTIC_NS,
                'uri' => array('@content' => self::DIAGNOSTIC_URI . $e[self::SRU_ERROR_CODE]),
                'message' => array('@content' => $e[self::SRU_ERROR_MESG]),
                'detail' => array('@content' => $e[self::SRU_ERROR_DETL]),
            )
        );

        return $data;
    }

//====================================================================================
//====================================================================================

    const GENERAL_SYSTEM_ERROR = 1;
    const SYSTEM_UNAVAILABLE = 2;
    const AUTHENTICATION_ERROR = 3;
    const UNSUPPORTED_OPERATION = 4;
    const UNSUPPORTED_VERSION = 5;
    const UNSUPPORTED_PARAMETER_VALUE = 6;
    const MANDATORY_PARAMETER_NOT_SUPPLIED = 7;
    const UNSUPPORTED_PARAMETER = 8;

    const ERR_CQL_VOCABULARY = 17; // misuse a deprecated diagnostic number
}


<?php

/**
 * @file
 * @brief
 * Implements SRU Server in a Drupal environment.
 *
 * Search/Retrieval via URL Server.
 * Requires the CQL module to process search instructions.
 *
 * @package     sru
 * @subpackage
 * @author
 */

/**
 * Menu callback for the settings form.
 */
function sru_get_admin_form() {
  $form['sru'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['sru']['sru_schema_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Schema location'),
    '#description' => t('Specify schema location.'),
    '#default_value' => variable_get('sru_schema_location', "http://{$_SERVER['HTTP_HOST']}"),
    '#required' => TRUE,
  );

  $form['sru']['sru_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorisation key'),
    '#description' => t('SRU auth key for accessing the SRU server. Available tokens: !user, !pass'),
    '#default_value' => variable_get('sru_access_key', 'username=!name:password=!pass:nso'),
    '#required' => TRUE,
  );

  $options  = sru_get_file_schemas();
  $form['sru']['sru_schema_file'] = array(
    '#type' => 'radios',
    '#title' => t('Default schema file'),
    '#description' => t('Default SRU schema file.'),
    '#default_value' => variable_get('sru_schema_file', key($options)),
    '#options' => $options,
    '#required' => TRUE,
  );

/*
  $form['sru']['sru_depository_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Depository name'),
    '#description' => t('See: !url', array('!url' => l('RFC4287', 'http://tools.ietf.org/html/rfc4287#section-4.2.14'))),
    '#default_value' => variable_get('sru_depository_name', ''),
    '#required' => TRUE,
  );
*/

  return system_settings_form($form);
}

/**
 * Validate form callback
 */
function sru_get_admin_form_validate($form, $form_state) {
    $values = $form_state['values'];
    if (!valid_url($values['sru_schema_location'], TRUE)) {
        form_set_error('homepage', t('The URL of your schema location is not valid. Remember that it must be fully qualified, i.e. of the form <code>http://example.com/directory</code>.'));
    }
}


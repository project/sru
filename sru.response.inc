<?php

/**
 * @file
 * @brief
 * Implements SRU Server in a Drupal environment.
 *
 * Abstract response class for SRU requests
 *
 * @package 	sru
 * @subpackage	
 * @author		
 */

_sru_load_error_file();

class sru_defaults {
    const numberOfRecords = 10;
    const sortSchema = '' ;
    const retrieveSchema = SRU_SCHEMAS_NS_XSD;
    const resultSetTTL = 300;
    const stylesheet = '' ;
    const recordPacking = 'xml' ;
    const numberOfTerms = 20;
    const maximumRecords = 20;

    static private $supports = 'resultSets scan relation:is relation:isnt relation:isallof relation:isanyof relation:isnoneof boolean:xor' ;

    static public function supported() { return self::$supports; }
}


abstract class sru_response {
    protected $sru;
    protected $dtd;
    protected $host;
    protected $port;
    private $error = FALSE;

    public function __construct($sru) {
        $this->sru = $sru;
        $this->dtd = url('sru/dtd/0.0/', array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE));

        $url = str_replace('//','/',url('', array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE)));
        $url = str_replace('//','/',url('', array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE)));
        list($a,$host,$z) = explode('/',$url);
        list($host,$port) = explode(':',$host);
        if ($port=='') $port = '80' ;
        $this->host = $host;
        $this->port = $port;

        if ($sru->operation === FALSE) {
            $this->add_error(sru_error::MANDATORY_PARAMETER_NOT_SUPPLIED, t('Missing mandatory parameter'), 'operation');
        }
        if ($sru->version === FALSE) {
            $this->add_error(sru_error::MANDATORY_PARAMETER_NOT_SUPPLIED, t('Missing mandatory parameter'), 'version');
        }

    }

/**
 * If there's an error during the processing then we want to
 * store it so that we report the error when the output string
 * is requested instead of anything else. Multiple errors are
 * accepted.
 */

    final protected function add_error($code, $msg = NULL, $detail = NULL) {
        if ($this->error === FALSE) {
            $this->error = new sru_error($this->sru);
        }
        $this->error->add($code, $msg, $detail);
    }

/**
 * Converting the sru_response to a string means converting it into
 * the output format, usually XML. We also have the situation where,
 * if an error occurred, we want to report that instead using the
 * sru_error object.
 */


    final public function __toString() {
        if ($this->error === FALSE) {
            $op = $this->response();
        } else {
            $op = $this->error->response();
        }
        return $op;
    }

    //---------------------------------------------

    /**
     * Generate the XML for the explain response
     */

    abstract protected function response();

}


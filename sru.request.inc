<?php

/**
 * @file
 * @brief
 * Implements SRU Server in a Drupal environment.
 *
 * SRU Request Class.
 *
 * This class handles any actual SRU request and
 * creates objects depending on what operation
 * is required
 *
 * @package 	sru
 * @subpackage
 * @author
 */

class sru_request {
    const baseVersion = 1.1;

    private $operation = NULL;
    private $version = NULL;
    private $query = NULL;
    private $startrecord = 1;
    private $maximumrecords = 10;
    private $recordpacking = 'xml' ;
    private $recordxpath = 'sru' ;
    private $resultsetttl = 0;
    private $sortkeys = array();
    private $stylesheet = '' ;
    private $extraRequestData = array();

    public function __construct($args) {
        unset($args['q']);
        foreach ($args as $var => $val) {
            $var = strtolower($var);
            if ($var=='version') {
                $this->version = ((float) $val < (float) sru_request::baseVersion) ? (float) $val : (float) sru_request::baseVersion;
            } else if ($var{0} == 'x') {
                $this->extraRequestData[$var] = $val;
            } else {
                $this->$var = $val;
            }
        }
    }

/**
 * Send the reply to the client, assumes XML
 *
 * Inserts the header content type and puts the XML spec at the top
 * and the stylesheet if specified
 */

    private function send($op) {
        $extras = '' ;
        if ($xsl = $this->stylesheet) {
            $ext = substr($xsl, strrpos($xsl,'.') + 1);
            $extras = sprintf("<?xml-stylesheet type='text/%s' href='%s' ?>\r\n", $ext, $xsl);
        }
        sru_request::sendxml($op, $extras);
    }

    static function sendxml($op, $extras = '') {
        header('Content-Type: text/xml; charset=utf-8');
        $xml = "<?xml version='1.0' encoding='UTF-8'?>\r\n";
        $xml .= $extras;
        print $xml . $op;
        exit;
    }

//-----------------------------------------------------------------

/**
 * Output this sru_request class as an HTML table
 */

    public function __toString() {
        $str = '<p>CQL_request<br /><table>';
        foreach ($this as $var => $val) {
            $str .= "<tr><td>{$var}</td><td>".($val).'</td></tr>' ;
        }
        return $str.'</table></p>' ; // FIXME: convert into Drupal table theme
    }

/**
 * Allow vars to be got, but not set
 */

    public function __get($var) {
        return $this->$var;
    }

    /**
     * Given the request, find a module to process it
     * and get it to issue the appropriate response
     */
    public function response() {
        // module_invoke_all('sru_auth'); // additional sru authentication conditions
        $results = module_invoke_all('sru', strtolower($this->operation));

        if (!empty($results)) {
            foreach ($results as $module => $data) {
                if ($file = $data['file']) {
                  require_once $file;
                }
                $class = $data['class'];
                $op = new $class($this);
                break;
            }
        } else {
            _sru_load_error_file();
            $op = new sru_error($this, sru_error::UNSUPPORTED_OPERATION, t('Operation not supported'), $this->operation);
        }
        $this->send($op);
    }
}


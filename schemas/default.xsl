<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:template match='//srw:records/'>
	<div class='node-records'>
		<xsl:for-each match='//node/'>
			<xsl:apply-templates select='//title' />
			<xsl:apply-templates select='//teaser' />
		</xsl:for-each>
	</div>
</xsl:template>

<xsl:template match='title'>
	<div class='node-title'>
		<xsl:text>Title: </xsl:text>
		<xsl:value-of select='.' />
	</div>
</xsl:template>

<xsl:template match='teaser'>
	<div class='node-teaser'>
		<xsl:text>Teaser: </xsl:text>
		<xsl:value-of select='.' />
	</div>
</xsl:template>
</xsl:stylesheet>

